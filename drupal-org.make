; One Profile make file
core = "7.x"
api = "2"
; comment this in to use in local development
; projects[drupal][version] = "7.x"

; Modules

projects[location][version] = "3.0-alpha1"
projects[location][subdir] = "contrib"

projects[ctools][version] = "1.2"
projects[ctools][subdir] = "contrib"

projects[commerce][version] = "1.5"
projects[commerce][subdir] = "contrib"

projects[context][version] = "3.0-beta6"
projects[context][subdir] = "contrib"

projects[date][version] = "2.6"
projects[date][subdir] = "contrib"

projects[partial_date][version] = "1.0-beta1"
projects[partial_date][subdir] = "contrib"

projects[features][version] = "1.0"
projects[features][subdir] = "contrib"

projects[feeds][version] = "2.0-alpha7"
projects[feeds][subdir] = "contrib"

projects[addressfield][version] = "1.0-beta3"
projects[addressfield][subdir] = "contrib"

projects[field_group][version] = "1.1"
projects[field_group][subdir] = "contrib"

projects[inline_entity_form][version] = "1.1"
projects[inline_entity_form][subdir] = "contrib"

projects[link][version] = "1.1"
projects[link][subdir] = "contrib"

projects[file_entity][version] = "2.0-unstable7"
projects[file_entity][subdir] = "contrib"

projects[media][version] = "2.0-unstable7"
projects[media][subdir] = "contrib"

projects[media_youtube][version] = "2.0-rc2"
projects[media_youtube][subdir] = "contrib"

projects[diff][version] = "3.2"
projects[diff][subdir] = "contrib"

projects[entity][version] = "1.0"
projects[entity][subdir] = "contrib"

projects[form_builder][version] = "1.3"
projects[form_builder][subdir] = "contrib"

projects[job_scheduler][version] = "2.0-alpha3"
projects[job_scheduler][subdir] = "contrib"

projects[libraries][version] = "2.0"
projects[libraries][subdir] = "contrib"

projects[module_filter][version] = "1.7"
projects[module_filter][subdir] = "contrib"

projects[options_element][version] = "1.8"
projects[options_element][subdir] = "contrib"

projects[remote_stream_wrapper][version] = "1.0-beta4"
projects[remote_stream_wrapper][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[token][version] = "1.5"
projects[token][subdir] = "contrib"

projects[panelizer][version] = "2.0"
projects[panelizer][subdir] = "contrib"

projects[panels][version] = "3.3"
projects[panels][subdir] = "contrib"

projects[rules][version] = "2.2"
projects[rules][subdir] = "contrib"

projects[blogapi_new][version] = "1.0-beta1"
projects[blogapi_new][subdir] = "contrib"

projects[wysiwyg][version] = "2.2"
projects[wysiwyg][subdir] = "contrib"

projects[wysiwyg_template][version] = "2.9"
projects[wysiwyg_template][subdir] = "contrib"

projects[views][version] = "3.5"
projects[views][subdir] = "contrib"

projects[views_slideshow][version] = "3.0"
projects[views_slideshow][subdir] = "contrib"

projects[fivestar][version] = "2.0-alpha2"
projects[fivestar][subdir] = "contrib"

projects[votingapi][version] = "2.10"
projects[votingapi][subdir] = "contrib"

projects[webform][version] = "3.18"
projects[webform][subdir] = "contrib"

projects[panels_extra_layouts][version] = "1.5"
projects[panels_extra_layouts][subdir] = "contrib"

projects[commerce_autosku][version] = "1.1"
projects[commerce_autosku][subdir] = "contrib"

projects[feeds_tamper][version] = "1.0-beta4"
projects[feeds_tamper][subdir] = "contrib"

projects[ds][version] = "2.2"
projects[ds][subdir] = "contrib"

; TODO modules without versions
projects[one][version] = "" ; TODO add version
projects[one][subdir] = "custom"

; Themes
; corporateclean
projects[corporateclean][type] = "theme"
projects[corporateclean][version] = "2.1"
projects[corporateclean][subdir] = "contrib"

; Libraries
libraries[profiler][directory_name] = "profiler"
libraries[profiler][type] = "library"
libraries[profiler][destination] = "libraries"
libraries[profiler][download][type] = "get"
libraries[profiler][download][url] = "http://ftp.drupal.org/files/projects/profiler-7.x-2.x-dev.tar.gz"

; Patches
projects[feeds][patch][] = "http://drupal.org/files/feeds-1127696-multiple-feeds-per-node-drush-make-8-for-alpha7-44.patch.patch"
